#!/bin/bash
#Marko Mandel
#Skript loob uue veebikodu
#NB! Lubatud symbolid URL-is on ainult a-z, 0-9 ja / 
#Lisaks on lubatud domeeninime eraldamiseks ka punkt
#
#Muutujate eesm2rk:
# WWW='www.'
# SAIT=saidinimi
# SUFIKS='.ee'
#

#kontrollib kas skript on k2ivitatud juurkasutaja 6igustes.
if [ $UID -ne 0 ]; then
    echo "Viga: K2ivita skript juurkasutaja 6igustes"
    exit 1
fi


#Kontrollib kas v6tmete arv on ikka 1. Kui ei ole, kuvab help message ja v2ljub.
if [ $# -ne 1 ]; then
    echo "Kasutamine: $0 www.minuveebisait.ee"
    exit 1
fi

SAIT=$1


#kontrollib kas see on korrektne URL-i vorming
#kas algab www.-ga.
if [[ ${SAIT:0:4} == "www." ]]; then             
      WWW=${SAIT:0:4}         #www.
      SAIT=${SAIT:4} 
else
      WWW="" #"www."
fi      
 


#kas l6pus on punkt ja 2 t2hte ning kas on ikka m6ni alfanumeeriline symbol domeeni nimes (et www.ee viga ei tuleks)    
i=$((${#SAIT}))          #SAIT stringi pikkus   
if ! [[ "${SAIT:$(($i-2)):2}" =~ [^a-z] || "${SAIT:$(($i-3)):1}" != "." || "${SAIT:$(($i-4)):1}" =~ [^a-z0-9] ]]; then 
    SUFIKS=${SAIT:$(($i-3)):3}
    SAIT=${SAIT:0:$(($i-3))} 
else
     echo "Viga: Vigane domeeni sufiks"
     exit 1
fi


#kontrollib kas saidi nimes sisaldub mittelubatud symboleid
if [[ $SAIT =~ [^a-z0-9/] ]]; then                
     echo "Viga: Lubamatud symbolid domeeninimes"
     exit 1
fi
echo "Sisend on reeglitele vastav!"


#Kontrollib, kas apache2 on paigaldatud. Kui ei ole, paigaldab.
type apache2 > /dev/null 2>&1 

if [ $? -ne 0 ]; then
    echo "Paigaldan Apache..."    
    apt-get update > /dev/null 2>&1 && apt-get install apache2 -y > /dev/null 2>&1 || exit 1
fi


#Et saaks antud aadressile veebilehitsejaga minna, peab olema nimelahendus nii,
#et antud aadressile vastab meie ip aadress. Selleks muudame /etc/hosts faili.
echo "127.0.0.1 $WWW$SAIT$SUFIKS" >> /etc/hosts
echo "127.0.0.1 $SAIT$SUFIKS" >> /etc/hosts  
echo "Hosts fail on edukalt muudetud."   




#loob antud domeenile kausta
mkdir -p /var/www/"$WWW$SAIT$SUFIKS"

#kontrollib kas vaikimisi index.html fail on olemas
if [ ! -f /var/www/html/index.html ]; then
    echo "Viga: Ei suuda leida vaikimisi index.html faili!!!"
fi

#kopeerib vaikimisi indeks faili uue domeeni kausta 
cp /var/www/html/index.html /var/www/"$WWW$SAIT$SUFIKS"


#j2rgnev tekst asendatakse index.html failis uue saidi nimega ehk muudetakse default faili.
VANA="Apache2 Ubuntu Default Page"
sed -i "s/$VANA/$SAIT/g" /var/www/"$WWW$SAIT$SUFIKS"/index.html > /dev/null 2>&1





#Kopeerib vaikimisi conf faili
cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/"$WWW$SAIT$SUFIKS".conf

#Asendab conf failis ServerName rea
sed -i "s/#ServerName www.example.com/ServerName $WWW$SAIT$SUFIKS/g" /etc/apache2/sites-available/"$WWW$SAIT$SUFIKS".conf > /dev/null 2>&1

#Asendab conf failis DocumentRoot rea
sed -i "s/DocumentRoot \/var\/www\/html/DocumentRoot \/var\/www\/$WWW$SAIT$SUFIKS/g" /etc/apache2/sites-available/"$WWW$SAIT$SUFIKS".conf > /dev/null 2>&1

#Kuna antud virtuaalmasin ei loo ise automaatselt linki, siis tuleb see luua ise
ln -s /etc/apache2/sites-available/"$WWW$SAIT$SUFIKS".conf /etc/apache2/sites-enabled/"$WWW$SAIT$SUFIKS".conf 




echo "Apache restartimine..."
service apache2 restart > /dev/null 2>&1

echo "Saidi $WWW$SAIT$SUFIKS server on edukalt loodud!"



