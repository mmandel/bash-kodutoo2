#!/bin/bash
#Marko Mandel
#Skript loob uue veebikodu
#


#kontrollib kas skript on k2ivitatud juurkasutaja 6igustes.
if [ $UID -ne 0 ]; then
    echo "K2ivita skript juurkasutaja 6igustes"
    exit 1
fi


#Kontrollib kas v6tmete arv on ikka 1. Kui ei ole, kuvab help message ja v2ljub.
if [ $# -ne 1 ]; then
    echo "Kasutamine: $0 www.minuveebisait.ee"
    exit 1
fi

SAIT=$1

#kontrollib kas see on korrektne URL-i vorming

#kas algab www.-ga. kas viies char on alfanumeeriline
#kas algab www.-ga. kas viies char on alfanumeeriline
if [[ ${SAIT:0:4} == "www." ]]; then             
      WWW=${SAIT:0:4}
      SAIT=${SAIT:4}
       echo "www on"  
else
      WWW="www."
fi      
      
 # echo $WWW    
#echo $SAIT



#echo "www on ja alfa1 on ok"
#echo "------------------------------------"


#kas l6pus on punkt ja 2 t2hte     
i=$((${#SAIT}))          #SAIT stringi pikkus 
#echo "${SAIT:$(($i-2)):2}"
#echo "${SAIT:$(($i-3)):1}"
#echo "${SAIT:$(($i-4)):1}"
if ! [[ "${SAIT:$(($i-2)):2}" =~ [^a-z] || "${SAIT:$(($i-3)):1}" != "." || "${SAIT:$(($i-4)):1}" =~ [^a-z0-9] ]]; then 
    echo "domeeni sufiks ok" 
    SUFIKS=${SAIT:$(($i-3)):3}
     SAIT=${SAIT:0:$(($i-3))} 
else
     echo "Vigane domeeni sufiks"
     exit 1
fi


#kontrollib kas saidinimes on ainult lubatud symbolid
#echo $SAIT
if [[ $SAIT =~ [^a-z0-9/] ]]; then 
     echo "Lubamatud symbolid domeeninimes"
     exit 1
fi
echo "-----------------------------------------------sisendikontroll finished"

#Kontrollib, kas apache2 on paigaldatud. Kui ei ole, paigaldab.
type apache2 > /dev/null 2>&1 

if [ $? -ne 0 ]; then
    echo "Paigaldan Apache..."    
    apt-get update > /dev/null 2>&1 && apt-get install apache2 -y > /dev/null 2>&1 || exit 1
fi


#Et saaks antud aadressile veebilehitsejaga minna, peab olema nimelahendus nii,
#et antud aadressile vastab meie ip aadress. Selleks muudame /etc/hosts faili.
echo "127.0.0.1 $WWW$SAIT$SUFIKS" >> /etc/hosts
echo "127.0.0.1 $SAIT$SUFIKS" >> /etc/hosts  
echo "Hosts fail on edukalt muudetud"    

#loob antud domeenile kausta
mkdir -p /var/www/"$WWW$SAIT$SUFIKS"

#kopeerib vaikimisi indeks faili uue domeeni kausta  #TODO kontrollida kas html fail on olemas
#kui ei leia siis: Ei leia default index.html faili yles
cp /var/www/html/index.html /var/www/"$WWW$SAIT$SUFIKS"


VANA="Apache2 Ubuntu Default Page"


#TODO - siin on j2rg

sed -i "s/$VANA/$SAIT/g" /var/www/"$WWW$SAIT$SUFIKS"/index.html
#sed -i 's/[$VANA]\/$*.^|[$SAIT]/\\&/g' /var/www/"$WWW$SAIT$SUFIKS"/index.html

#'s/[]\/$*.^|[]/\\&/g'

#mv /var/www/index.html /var/www/"$WWW$SAIT$SUFIKS"


#mkdir -p /var/www/www
#cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/"$WWW$SAIT$SUFIKS".conf

#sed -i "s/\/var\/www\/html/\/var\/www\/"$WWW$SAIT$SUFIKS"/g" /etc/apache2/sites-available/"$WWW$SAIT$SUFIKS"

#DocumentRoot /var/www/html
#cp /etc/apache2/sites-enabled/000-default.conf /etc/apache2/sites-enabled/"$WWW$SAIT$SUFIKS"

#sed -i "s/\/var\/www\/html/\/var\/www\/"$WWW$SAIT$SUFIKS"/g" /etc/apache2/sites-enabled/"$WWW$SAIT$SUFIKS"

cat >> /etc/apache2/sites-available/000-default.conf  <<LOPP    
<VirtualHost *:80>
        ServerName $SAIT$SUFIKS
        ServerAlias $WWW$SAIT$SUFIKS
        DocumentRoot /var/www/$WWW$SAIT$SUFIKS
        ErrorLog /var/log/apache2/$WWW$SAIT$SUFIKS.log
        CustomLog /var/log/apache2/$WWW$SAIT$SUFIKS.access.log combined
</VirtualHost>
LOPP


echo "Restardin Apache2..."
service apache2 restart  > /dev/null 2>&1
echo "valma"

